/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controler;

import entity.Variable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

/**
 *
 * @author SONY
 */
public class VariableC extends ControllerBase{
    
    private EntityManager em;
    
    public VariableC(){
        super();
    }
    
    public VariableC(String pu){
        super(pu);
    }
    
    public Variable findVariableByNombre(String nombre){
        em =  getEntityManager();
        try{
            Query q = em.createNamedQuery("Variable.findByNombre",Variable.class);
            q.setParameter("nombre", nombre);
            
            return (Variable) q.getSingleResult();
        }catch(Exception e){
            if(e instanceof NoResultException){
                System.err.println("Sin resultado findVariableByNombre");
            }else{
                e.printStackTrace();
            }
            return null;
        }finally{
            em.close();
        }
    }
            
    public Variable findVariableById(int id){
        em =  getEntityManager();
        try{
            Query q = em.createNamedQuery("Variable.findById",Variable.class);
            q.setParameter("id", id);
            
            return (Variable) q.getSingleResult();
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }finally{
            em.close();
        }
    }   

    public Variable merge(Variable v){
        em =  getEntityManager();
        Variable mergedVariable = v;
        try{
            em.getTransaction().begin();
            mergedVariable = em.merge(v);
            em.getTransaction().commit();
            return mergedVariable;
        }catch(Exception e){
            e.printStackTrace();
            em.getTransaction().rollback();
            return mergedVariable;
        }finally{
            em.close();
        }
    }
}

