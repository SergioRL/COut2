/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controler;

import common.Constantes;
import common.Utilidades;
import entity.Muestra;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 *
 * @author SONY
 */
public class MuestraC extends ControllerBase{
    
    private EntityManager em;
    
    public MuestraC(){
        super();
    }
    
    public MuestraC(String pu){
        super(pu);
    }
    
    public List<Muestra> getAllMuestra(){
        em =  getEntityManager();
        try{
            Query q = em.createNamedQuery("Muestra.findAll");
            
            return (List<Muestra>) q.getResultList();
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }finally{
            em.close();
        }
    }
    
     public Date getUltimaFecha() {
        em = getEntityManager();
        try {
            Query q = em.createQuery("Select MAX(m.fecha) FROM Muestra m WHERE NOT m.fecha IS NULL");

            return (Date) q.getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            em.close();
        }
    }
    
     public Date getPrimeraFecha() {
        em = getEntityManager();
        try {
            Query q = em.createQuery("Select MIN(m.fecha) FROM Muestra m WHERE NOT m.fecha IS NULL");

            return (Date) q.getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            em.close();
        }
    }
     
    public Muestra getMuestraById(int id){
        em =  getEntityManager();
        try{
            Query q = em.createNamedQuery("Muestra.findById");
            q.setParameter("id", id);
            
            return (Muestra) q.getSingleResult();
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }finally{
            em.close();
        }
    } 
    
    public List<Muestra> findLazy(int firstResult, int resultCount, Date desde, Date hasta, Double minVal, Double maxVal, Integer tipo, Date fechaLim) {
        EntityManager em = getEntityManager();

        try {
            Map<String, Object> map = mapaVariables(desde, hasta, minVal, maxVal, tipo);
            String sql = "SELECT m FROM Muestra m WHERE m.fecha <= :fechaLim ";

            String sql2 = condicionesBusquedaSQL(desde, hasta, minVal, maxVal, tipo);
            sql += sql2;
            sql += " ORDER BY m.fecha DESC";

            javax.persistence.Query q = em.createQuery(sql);
            
            q.setParameter("fechaLim", fechaLim);
            
            if (!Utilidades.estaVacio(sql2) && map.keySet() != null) {
                for (String s : map.keySet()) {
                    q.setParameter(s, map.get(s));
                }
            }

            if (firstResult >= 0) {
                q.setFirstResult(firstResult);
            }

            if (resultCount >= 0) {
                q.setMaxResults(resultCount);
            }

            return (List<Muestra>) q.getResultList();
        } catch (Exception e) {
            return null;
        } finally {
            em.close();
        }
    } 
    
    public Long findLazyCount(Date desde, Date hasta, Double minVal, Double maxVal, Integer tipo, Date fechaLim) {
        EntityManager em = getEntityManager();

        try {
            Map<String, Object> map = mapaVariables(desde, hasta, minVal, maxVal, tipo);
            String sql = "SELECT count(m) FROM Muestra m WHERE m.fecha <= :fechaLim ";

            String sql2 = condicionesBusquedaSQL(desde, hasta, minVal, maxVal, tipo);
            sql += sql2;

            javax.persistence.Query q = em.createQuery(sql);

            q.setParameter("fechaLim", fechaLim);
            
            if (!Utilidades.estaVacio(sql2) && map.keySet() != null) {
                for (String s : map.keySet()) {
                    q.setParameter(s, map.get(s));
                }
            }

            return (Long) q.getSingleResult();
        } catch (Exception e) {
            return null;
        } finally {
            em.close();
        }

    }
    
    public Date findUltimaFecha(Date desde, Date hasta, Double minVal, Double maxVal, Integer tipo, Date fechaLim) {
        EntityManager em = getEntityManager();

        try {
            Map<String, Object> map = mapaVariables(desde, hasta, minVal, maxVal, tipo);
            String sql = "SELECT MAX(m.fecha) FROM Muestra m WHERE m.fecha <= :fechaLim ";

            String sql2 = condicionesBusquedaSQL(desde, hasta, minVal, maxVal, tipo);
            sql += sql2;

            javax.persistence.Query q = em.createQuery(sql);

            q.setParameter("fechaLim", fechaLim);
            
            if (!Utilidades.estaVacio(sql2) && map.keySet() != null) {
                for (String s : map.keySet()) {
                    q.setParameter(s, map.get(s));
                }
            }

            return (Date) q.getSingleResult();
        } catch (Exception e) {
            return null;
        } finally {
            em.close();
        }

    }
    
    public Date findPrimeraFecha(Date desde, Date hasta, Double minVal, Double maxVal, Integer tipo, Date fechaLim) {
        EntityManager em = getEntityManager();

        try {
            Map<String, Object> map = mapaVariables(desde, hasta, minVal, maxVal, tipo);
            String sql = "SELECT MIN(m.fecha) FROM Muestra m WHERE m.fecha <= :fechaLim ";

            String sql2 = condicionesBusquedaSQL(desde, hasta, minVal, maxVal, tipo);
            sql += sql2;

            javax.persistence.Query q = em.createQuery(sql);

            q.setParameter("fechaLim", fechaLim);
            
            if (!Utilidades.estaVacio(sql2) && map.keySet() != null) {
                for (String s : map.keySet()) {
                    q.setParameter(s, map.get(s));
                }
            }

            return (Date) q.getSingleResult();
        } catch (Exception e) {
            return null;
        } finally {
            em.close();
        }

    }
    
    public Muestra merge(Muestra m){
        em =  getEntityManager();
        try{
            em.getTransaction().begin();
            Muestra aux = em.merge(m);
            em.getTransaction().commit();
            return aux;
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }finally{
            em.close();
        }
    } 
    
    private Map<String, Object> mapaVariables(Date desde, Date hasta, Double minVal, Double maxVal, Integer tipo) {
        Map<String, Object> map = new HashMap<>();

        if (!(desde == null)) {
            map.put("desde", desde);
        }
        if (!(hasta == null)) {
            map.put("hasta", hasta);
        }
        if (!(minVal == null)) {
            map.put("minVal", minVal);
        }
        if (!(maxVal == null)) {
            map.put("maxVal", maxVal);
        }
        if (!(tipo == null)) {
            map.put("tipo", tipo);
        }

        return map;
    }
    
    private String condicionesBusquedaSQL(Date desde, Date hasta, Double minVal, Double maxVal, Integer tipo) {
        String sql = "";

        if (desde != null) {
            sql += " AND m.fecha >= :desde";
        }
        if ( hasta != null) {
            sql += " AND CAST(m.fecha as date) <= :hasta";
        }
        if ( minVal != null) {
            sql += " AND m.valor >= :minVal";
        }
        if ( maxVal != null) {
            sql += " AND m.valor <= :maxVal";
        }
        if ( tipo != null) {
            sql += " AND m.tipo.id = :tipo";
        }
        
        return sql;
    }
}
