/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controler;

import common.Constantes;
import entity.Perfil;
import java.util.ArrayList;
import java.util.List;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 *
 * @author SONY
 */
public class PerfilC extends ControllerBase{
    
    private EntityManager em;
    
    public PerfilC(){
        super();
    }
    
    public PerfilC(String pu){
        super(pu);
    }
    
    public List<Perfil> getAllPerfil(){
        em =  getEntityManager();
        try{
            Query q = em.createNamedQuery("Perfil.findAll");
            
            return (List<Perfil>) q.getResultList();
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }finally{
            em.close();
        }
    }
    
    public List<Perfil> getAllNoBloqueado(){
        em =  getEntityManager();
        try{
            Query q = em.createQuery("SELECT p FROM Perfil p WHERE NOT p.id = :id");
            q.setParameter("id", Constantes.ID_BLOQUEADO);
            
            return (List<Perfil>) q.getResultList();
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }finally{
            em.close();
        }
    }
    
    public Perfil getPerfilBloqeado(){
        em =  getEntityManager();
        try{
            Query q = em.createNamedQuery("Perfil.findById");
            q.setParameter("id", Constantes.ID_BLOQUEADO);
            
            return (Perfil) q.getSingleResult();
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }finally{
            em.close();
        }
    }
    
    public Perfil getPerfilById(int id){
        em =  getEntityManager();
        try{
            Query q = em.createNamedQuery("Perfil.findById");
            q.setParameter("id", id);
            
            return (Perfil) q.getSingleResult();
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }finally{
            em.close();
        }
    }
    
    public List<SelectItem> list2SelectItems(List<Perfil> lista){
        List<SelectItem> result = new ArrayList<SelectItem>();
        
        if(lista != null){
            for(Perfil p : lista){
                if(p != null){
                    result.add(new SelectItem(p.getId(), p.getNombre()));
                }
            }
        }
        
        return result;
    } 
}
