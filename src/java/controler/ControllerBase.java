/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controler;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author SONY
 */
public class ControllerBase {
    private static final String PU_NAME = "COut2PU";
      
    private EntityManagerFactory emf;
    
    public ControllerBase(){
        emf = Persistence.createEntityManagerFactory(PU_NAME);
    }
    
    public ControllerBase(String pu){
        emf = Persistence.createEntityManagerFactory(pu);
    }
    
    public EntityManager getEntityManager(){
        return emf.createEntityManager();
    }
    
    @Override
    public void finalize(){
        try{
            if(emf != null && emf.isOpen())
                emf.close();
        }catch (Throwable ex) {
            Logger.getLogger(ControllerBase.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            try {
                super.finalize();
            } catch (Throwable ex) {
                Logger.getLogger(ControllerBase.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
