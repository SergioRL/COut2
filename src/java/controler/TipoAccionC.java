/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controler;

import entity.TipoAccion;
import java.util.ArrayList;
import java.util.List;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 *
 * @author SONY
 */
public class TipoAccionC extends ControllerBase{
    private EntityManager em;
    
    public TipoAccionC(){
        super();
    }
    
    public TipoAccionC(String pu){
        super(pu);
    }
    
    public List<TipoAccion> findAllTipoAccion(){
        em = getEntityManager();
        
        try{
            Query q = em.createNamedQuery("TipoAccion.findAll");
            
            return (List<TipoAccion>) q.getResultList();
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }
    
    public TipoAccion findTipoAccionById(int id){
        em = getEntityManager();
        
        try{
            Query q = em.createNamedQuery("TipoAccion.findById");
            q.setParameter("id", id);
            
            return (TipoAccion) q.getSingleResult();
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }
    
    public List<SelectItem> list2SelectItems(List<TipoAccion> lista){
        ArrayList<SelectItem> result = new ArrayList<>();
        
        if(lista != null){
            lista.stream().filter((tp) -> (tp != null)).forEach((tp) -> {
                result.add(new SelectItem(tp.getId(), tp.getNombre()));
            });
        }
        
        return result;
    } 

}
