/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controler;

import entity.Usuario;
import java.util.ArrayList;
import java.util.List;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

/**
 *
 * @author SONY
 */
public class UsuarioC extends ControllerBase{
    
    private EntityManager em;
    
    public UsuarioC(){
        super();
    }
    
    public UsuarioC(String pu){
        super(pu);
    }
    
    public List<Usuario> getAllUsuario(){
        em =  getEntityManager();
        try{
            Query q = em.createNamedQuery("Usuario.findAll");
            
            return (List<Usuario>) q.getResultList();
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }finally{
            em.close();
        }
    }
    
    public List<Usuario> getAllUsuarioAlfabetico(){
        em =  getEntityManager();
        try{
            Query q = em.createQuery("SELECT u FROM Usuario u ORDER BY u.identificacion");
            
            return (List<Usuario>) q.getResultList();
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }finally{
            em.close();
        }
    }
    
    public boolean existeUsuarioIdentificacion(String ident){
        em =  getEntityManager();
        try{
            Query q = em.createQuery("SELECT u FROM Usuario u WHERE u.identificacion = :identificacion");
            
            q.setParameter("identificacion",ident);
            
            List<Object> auxList ;
            
            try{
                auxList = q.getResultList();
            }catch(Exception e){
                auxList = null;
            }
            
            return auxList != null && auxList.size() > 0;
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }finally{
            em.close();
        }
    }
    
    public Usuario getUsuarioByCredenciales(String ident, String pass){
        em =  getEntityManager();
        try{
            Query q = em.createQuery("SELECT u FROM Usuario u WHERE u.identificacion = :identificacion AND u.password = :password");
            
            q.setParameter("identificacion",ident);
            q.setParameter("password",pass);
            
            return (Usuario) q.getSingleResult();
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }finally{
            em.close();
        }
    }
    
    public Usuario getUsuarioByIdentificacion(String ident){
        em =  getEntityManager();
        try{
            Query q = em.createQuery("SELECT u FROM Usuario u WHERE u.identificacion = :identificacion");
            
            q.setParameter("identificacion",ident);
            
            return (Usuario) q.getSingleResult();
        }catch(NoResultException e){
            e.printStackTrace();
            return null;
        }catch(Exception e){
            e.printStackTrace();
            throw e;
        }finally{
            em.close();
        }
    }
    
    public Usuario getUsuarioById(int id){
        em =  getEntityManager();
        try{
            Query q = em.createQuery("SELECT u FROM Usuario u WHERE u.id = :id",Usuario.class);
            
            q.setParameter("id",id);
            
            return (Usuario) q.getSingleResult();
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }finally{
            em.close();
        }
    }
    
    public Usuario merge(Usuario u){
        em =  getEntityManager();
        Usuario mergedUsuario = u;
        try{
            em.getTransaction().begin();
            mergedUsuario = em.merge(u);
            em.getTransaction().commit();
            return mergedUsuario;
        }catch(Exception e){
            e.printStackTrace();
            em.getTransaction().rollback();
            return mergedUsuario;
        }finally{
            em.close();
        }
    }
    
    public List<SelectItem> list2SelectItems(List<Usuario> lista){
        List<SelectItem> result = new ArrayList<>();
        
        if(lista != null){
            for(Usuario u : lista){
                if(u != null){
                    result.add(new SelectItem(u.getId(), u.getIdentificacion()+" - "+u.getPerfil().getNombre()));
                }
            }
        }
        
        return result;
    } 
}
