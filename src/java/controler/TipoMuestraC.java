/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controler;

import entity.TipoMuestra;
import java.util.ArrayList;
import java.util.List;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 *
 * @author SONY
 */
public class TipoMuestraC extends ControllerBase{
    private EntityManager em;
    
    public TipoMuestraC(){
        super();
    }
    
    public TipoMuestraC(String pu){
        super(pu);
    }
    
    public List<TipoMuestra> findAllTipoMuestra(){
        em = getEntityManager();
        
        try{
            Query q = em.createNamedQuery("TipoMuestra.findAll");
            
            return (List<TipoMuestra>) q.getResultList();
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }
    
    public TipoMuestra findTipoMuestraById(int id){
        em = getEntityManager();
        
        try{
            Query q = em.createNamedQuery("TipoMuestra.findById");
            q.setParameter("id", id);
            
            return (TipoMuestra) q.getSingleResult();
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }
    
    public List<SelectItem> list2SelectItems(List<TipoMuestra> lista){
        ArrayList<SelectItem> result = new ArrayList<>();
        
        if(lista != null){
            lista.stream().filter((tp) -> (tp != null)).forEach((tp) -> {
                result.add(new SelectItem(tp.getId(), tp.getDescripcion()));
            });
        }
        
        return result;
    } 

}