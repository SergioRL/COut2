/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controler;

import common.Utilidades;
import entity.Accion;
import entity.TipoAccion;
import entity.Usuario;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 *
 * @author SONY
 */
public class AccionC extends ControllerBase{
    private EntityManager em;
    
    public AccionC(){
        super();
    }
    
    public AccionC(String pu){
        super(pu);
    }
    
    public Accion findAccionById(int id){
        em = getEntityManager();
        
        try{
            Query q = em.createNamedQuery("Accion.findById");
            q.setParameter("id", id);
            
            return (Accion) q.getSingleResult();
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }
    
    public Accion findAccionByUsuario(Usuario usuario){
        em = getEntityManager();
        
        try{
            Query q = em.createNamedQuery("Accion.findByUsuario");
            q.setParameter("usuario", usuario);
            
            return (Accion) q.getSingleResult();
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }
    
    public Accion findAccionByIdUsuario(int idUsuario){
        em = getEntityManager();
        
        try{
            Query q = em.createQuery("SELECT a FROM Accion a WHERE a.usuario.id = :idUsuario");
            q.setParameter("idUsuario", idUsuario);
            
            return (Accion) q.getSingleResult();
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }
    
    public Accion merge(Accion accion){
        em = getEntityManager();
        
        try{
            em.getTransaction().begin();
            Accion accMerge = em.merge(accion);
            em.getTransaction().commit();
            return accMerge;
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }
    
    public List<Accion> findLazy(int firstResult, int resultCount, Date desde, Date hasta, Integer usuario, Integer tipo, Date fechaLim) {
        EntityManager em = getEntityManager();

        try {
            Map<String, Object> map = mapaVariables(desde, hasta, usuario, tipo);
            String sql = "SELECT a FROM Accion a WHERE a.fecha <= :fechaLim ";

            String sql2 = condicionesBusquedaSQL(desde, hasta, usuario, tipo);
            sql += sql2;
            sql += " ORDER BY a.fecha DESC";

            javax.persistence.Query q = em.createQuery(sql);

            q.setParameter("fechaLim", fechaLim);
            
            if (!Utilidades.estaVacio(sql2) && map.keySet() != null) {
                for (String s : map.keySet()) {
                    q.setParameter(s, map.get(s));
                }
            }

            if (firstResult >= 0) {
                q.setFirstResult(firstResult);
            }

            if (resultCount >= 0) {
                q.setMaxResults(resultCount);
            }

            return (List<Accion>) q.getResultList();
        } catch (Exception e) {
            return null;
        } finally {
            em.close();
        }
    } 
    
    public Long findLazyCount(Date desde, Date hasta, Integer usuario, Integer tipo, Date fechaLim) {
        EntityManager em = getEntityManager();

        try {
            Map<String, Object> map = mapaVariables(desde, hasta, usuario, tipo);
            String sql = "SELECT count(a) FROM Accion a WHERE a.fecha <= :fechaLim ";

            String sql2 = condicionesBusquedaSQL(desde, hasta, usuario, tipo);
            sql += sql2;

            javax.persistence.Query q = em.createQuery(sql);

            q.setParameter("fechaLim", fechaLim);
            
            if (!Utilidades.estaVacio(sql2) && map.keySet() != null) {
                for (String s : map.keySet()) {
                    q.setParameter(s, map.get(s));
                }
            }

            return (Long) q.getSingleResult();
        } catch (Exception e) {
            return null;
        } finally {
            em.close();
        }

    }
    
    private Map<String, Object> mapaVariables(Date desde, Date hasta, Integer usuario, Integer tipo) {
        Map<String, Object> map = new HashMap<>();

        if (!(desde == null)) {
            map.put("desde", desde);
        }
        if (!(hasta == null)) {
            map.put("hasta", hasta);
        }
        if (!(usuario == null)) {
            map.put("usuario", usuario);
        }
        if (!(tipo == null)) {
            map.put("tipo", tipo);
        }

        return map;
    }
    
    private String condicionesBusquedaSQL(Date desde, Date hasta, Integer usuario, Integer tipo) {
        String sql = "";

        if (desde != null) {
            sql += " AND a.fecha >= :desde";
        }
        if ( hasta != null) {
            sql += " AND CAST(a.fecha as date) <= :hasta";
        }
        if ( usuario != null) {
            sql += " AND a.usuario.id = :usuario";
        }
        if ( tipo != null) {
            sql += " AND a.tipo.id = :tipo";
        }

        return sql;
    }
    
}
