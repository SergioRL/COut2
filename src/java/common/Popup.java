/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package common;

import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;

/**
 *
 * @author SONY
 */
@ManagedBean
@ViewScoped
public class Popup implements Serializable{
    
    private static final String INFO_CLASS = "popupInfo";
    private static final String SUCCESS_CLASS = "popupSuccess";
    private static final String WARNING_CLASS = "popupWarning";
    private static final String ERROR_CLASS = "popupError";
    
    private static final String INFO_ICON = "fa fa-3x fa-info-circle";
    private static final String SUCCESS_ICON = "fa fa-3x fa-check-circle";
    private static final String WARNING_ICON = "fa fa-3x fa-exclamation-triangle";
    private static final String ERROR_ICON = "fa fa-3x fa-exclamation-circle";
    
    private static String cabecera;
    private static String cuerpo;
    private static String classPopup;
    private static String iconPopup;

    public static String getCabecera() {
        return cabecera;
    }

    public static String getCuerpo() {
        return cuerpo;
    }

    public static String getClassPopup() {
        return classPopup;
    }

    public static String getIconPopup() {
        return iconPopup;
    }
    
    public static void info(String cabecera, String cuerpo){
        classPopup = INFO_CLASS;
        iconPopup = INFO_ICON;
        mostrarPopup(cabecera, cuerpo);
    }
    
    public static void success(String cabecera, String cuerpo){
        classPopup = SUCCESS_CLASS;
        iconPopup = SUCCESS_ICON;
        mostrarPopup(cabecera, cuerpo);
    }
    
    public static void warning(String cabecera, String cuerpo){
        classPopup = WARNING_CLASS;
        iconPopup = WARNING_ICON;
        mostrarPopup(cabecera, cuerpo);
    }
    
    public static void error(String cabecera, String cuerpo){
        classPopup = ERROR_CLASS;
        iconPopup = ERROR_ICON;
        mostrarPopup(cabecera, cuerpo);
    }
    
    private static void mostrarPopup(String cabecera, String cuerpo){
        Popup.cabecera = cabecera;
        Popup.cuerpo = cuerpo;
        RequestContext.getCurrentInstance().execute("PF('popupInfo').show()");
    }
    
    public static void cerrarPopup(){
        RequestContext.getCurrentInstance().execute("PF('popupInfo').hide()");
    }
    
    public static void mostrarPopupTemplate(){
        RequestContext.getCurrentInstance().execute("PF('popupTemplate').show()");
    }
    
    public static void cerrarPopupTemplate(){
        RequestContext.getCurrentInstance().execute("PF('popupTemplate').hide()");
    }
}
