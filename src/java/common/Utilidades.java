/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package common;

import beans.administracion.EditarUsuario;
import controler.TipoAccionC;
import controler.TipoMuestraC;
import entity.Accion;
import entity.Muestra;
import entity.TipoAccion;
import entity.TipoMuestra;
import entity.Usuario;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.Date;
import javax.activation.MimetypesFileTypeMap;
import javax.faces.context.FacesContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author SONY
 */
public class Utilidades {
    public static boolean estaVacio(String s){
        return s == null || s.trim().length() == 0;
    }
    
    public static Muestra crearNuevaMuestra(TipoMuestra tipoMuestra, Date fechaMuestra, BigDecimal valorMuestra){
        Muestra result = new Muestra();
        result.setFecha(fechaMuestra);
        result.setTipo(tipoMuestra);
        result.setValor(valorMuestra);
        return result;
    }
    
    public static  Muestra crearNuevaMuestra(int tipoMuestra, Date fechaMuestra, BigDecimal valorMuestra){
        TipoMuestraC tmC = new TipoMuestraC();
        TipoMuestra tipM = tmC.findTipoMuestraById(tipoMuestra);
        return crearNuevaMuestra(tipM, fechaMuestra, valorMuestra);
    }
    
    public static Accion crearNuevaAccion(TipoAccion tipoAccion, String descipcion, Date fechaAccion, Usuario usuario){
        Accion result = new Accion();
        result.setDescripcion(descipcion);
        result.setFecha(fechaAccion);
        result.setUsuario(usuario);
        result.setTipo(tipoAccion);
        return result;
    }
    
    public static Accion crearNuevaAccion(int tipoAccion, String descipcion, Date fechaAccion, Usuario usuario){
        TipoAccionC taC = new TipoAccionC();
        TipoAccion tipAcc = taC.findTipoAccionById(tipoAccion);
        return crearNuevaAccion(tipAcc, descipcion, fechaAccion, usuario);
    }
    
    public static Accion crearNuevaAccion(String descipcion, Date fechaAccion, Usuario usuario){
        return crearNuevaAccion(null, descipcion, fechaAccion, usuario);
    }
    
    public static Accion crearNuevaAccionActual(TipoAccion tipoAccion, String descipcion, Usuario usuario){
        return crearNuevaAccion(tipoAccion, descipcion, new Date(), usuario);
    }
    
    public static Accion crearNuevaAccionActual(int tipoAccion, String descipcion, Usuario usuario){
        return crearNuevaAccion(tipoAccion, descipcion, new Date(), usuario);
    }
    
    public static Accion crearNuevaAccionActual(String descipcion, Usuario usuario){
        return crearNuevaAccion(null, descipcion, new Date(), usuario);
    }
    
    public static Object buscarBean(String nombreBean){
        try{
            return FacesContext.getCurrentInstance().getApplication().getELResolver().getValue(FacesContext.getCurrentInstance().getELContext(), null, nombreBean);            
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }
    
    public static String getExtension(String file) {
        String[] archivoSplit = file.split("\\.");
        return archivoSplit[archivoSplit.length - 1];
    }
    
    public static void descargarFichero(String file, String nombre) {

        String extension = getExtension(file);//archivoSplit[archivoSplit.length - 1]; //obtiene la extensi�n, la �ltima particula separada por

        String mimetype;
        try {
            FacesContext.getCurrentInstance().renderResponse();
            HttpServletResponse responseF = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
            //  HttpServletResponse responseA =  (HttpServletResponse)ADFContext.getCurrent().getEnvironment().getResponse();

            HttpServletResponse response = responseF;
            response.setHeader("Content-Disposition", "attachment;filename =" + nombre);
            //  response.setContentType("text/plain");  //"text/plain","image/tiff","image/jpeg"

            File f = new File(file);
            mimetype = new MimetypesFileTypeMap().getContentType(f);

            response.setContentType(mimetype);
            InputStream in = new FileInputStream(f);

            ServletOutputStream outs = response.getOutputStream();

            int i = 0;

            while ((i = in.read()) > -1) {

                outs.write(i);

            }
            outs.flush();
            outs.close();
            in.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        FacesContext.getCurrentInstance().responseComplete();
    }
}
