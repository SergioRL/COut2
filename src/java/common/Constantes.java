/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package common;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author SONY
 */

public class Constantes {
    
    //ID PERFILES
    public static final Integer ID_ADMINISTRADOR = 1;
    public static final Integer ID_OPERADOR = 2;
    public static final Integer ID_CONSULTOR = 3;
    public static final Integer ID_BLOQUEADO = 4;
    
    //VARIABLES BD
    public static final String VARIABLE_URL_RASPBERRY = "URL_RASPBERRY";
    public static final String VARIABLE_INTERVALO_MUESTRA = "INTERVALO_MUESTRA";
    public static final String VARIABLE_NIVEL_MAXIMO = "NIVEL_MAXIMO";
//    public static final String ESTADO_AUTOMATICO = "ESTADO_AUTOMATICO";
//    public static final String ESTADO_MANUAL = "ESTADO_MANUAL";
    
    //VARIABLES POR DEFECTO
    public static final String POR_DEFECTO_URL_RASPBERRY = "";
    public static final String POR_DEFECTO_INTERVALO_MUESTRA = "60";
    public static final String POR_DEFECTO_NIVEL_MAXIMO = "200";
    
    //VALORES LIMITE
    public static final int MIN_INTERVALO_MUESTRA = 15;
    public static final Double MIN_NIVEL_MAXIMO = 0.0;
    
    //ENUM ESTADOS
    public static enum EstadoAutomatico {ENCENDIDO,APAGADO,DESCONOCIDO}
    public static enum EstadoActuador {ABIERTO,CERRADO,DESCONOCIDO}

    //TIPO ACCION
    public static final Integer ID_CREACION_USUARIO = 1;
    public static final Integer ID_BLOQUEO_USUARIO = 2;
    public static final Integer ID_DESBLOQUEO_USUARIO = 3;
    public static final Integer ID_CAMBIO_PERFIL_USUARIO = 4;
    public static final Integer ID_CAMBIO_CREDENCIALES_USUARIO = 5;
    public static final Integer ID_CAMBIO_URL_RASPBERRY = 6;
    public static final Integer ID_CAMBIO_TIEMPO_REPORTE = 7;
    public static final Integer ID_CABIO_NIVEL_CO2 = 8;
    public static final Integer ID_APERTURA_MANUAL = 9;
    public static final Integer ID_CIERRE_MANUAL = 10;
    public static final Integer ID_ENCENDIDO_AUTOMATICO = 11;
    public static final Integer ID_APAGADO_AUTOMATICO = 12;
    
    //TIPO MUESTRA
    public static final Integer ID_NIVEL_CO2 = 1;
}
