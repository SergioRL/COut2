/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package common.lazyModel;

import common.Popup;
import controler.AccionC;
import entity.Accion;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

/**
 *
 * @author SONY
 */
public class LazyDatosAcciones extends LazyDataModel<Accion> {
    
    Date desde,hasta,creacion;
    Integer usuario,tipo;
     
    private List<Accion> datasource;
    private AccionC aC;
    
    private boolean vacio,nuevaCreacion;
    
    public LazyDatosAcciones(Date desde, Date hasta, Integer usuario, Integer tipo, boolean nuevaCreacion, boolean vacio){
        this.desde = desde;
        this.hasta = hasta;
        this.usuario = usuario;
        this.tipo = tipo;
        this.vacio = vacio;  
        this.nuevaCreacion = nuevaCreacion;
        
        creacion = new Date();
        
        aC = new AccionC();
    }
    
    public LazyDatosAcciones(Date desde, Date hasta, Integer usuario, Integer tipo, boolean nuevaCreacion){
        this(desde, hasta, usuario, tipo, nuevaCreacion, false);
        
    } 
    
    @Override
    public Accion getRowData(String rowKey) {
        for(Accion accion : datasource) {
            if(accion.getId().toString().equals(rowKey))
                return accion;
        }
 
        return null;
    }
 
    @Override
    public Object getRowKey(Accion accion) {
        return accion.getId();
    }
    
    @Override
    public List<Accion> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters) {
        
        if(aC == null){
            aC = new AccionC();
        }
        
        if(vacio){
            datasource = new ArrayList<>();
            this.setRowCount(0);
        }else{
            datasource = aC.findLazy(first, pageSize, desde, hasta, usuario, tipo, creacion);
            if(datasource == null){
                setRowCount(0);
            }else{
                setRowCount(aC.findLazyCount(desde, hasta, usuario, tipo, creacion).intValue());
            }
            
            if(this.getRowCount() <= 0 && !nuevaCreacion){
                Popup.info("Sin resultados","No se han encontrado acciones que cumplan los parametros especificados.");
            }
        }
        
        return datasource;
    }
    
}
