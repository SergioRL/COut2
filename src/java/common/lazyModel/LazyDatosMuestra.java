/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package common.lazyModel;

import common.Popup;
import controler.MuestraC;
import entity.Muestra;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

/**
 *
 * @author SONY
 */
public class LazyDatosMuestra extends LazyDataModel<Muestra> {
    
    Date desde,hasta,creacion;
    Double minVal,maxVal;
    Integer tipo;
     
    private List<Muestra> datasource;
    private MuestraC mC;
    
    private boolean vacio,nuevaCreacion;
    
    public LazyDatosMuestra(Date desde, Date hasta, Double minVal, Double maxVal, Integer tipo, boolean nuevaCreacion, boolean vacio){
        this.desde = desde;
        this.hasta = hasta;
        this.minVal = minVal;
        this.maxVal = maxVal;
        this.tipo = tipo;
        this.vacio = vacio;  
        this.nuevaCreacion = nuevaCreacion;
        
        creacion = new Date();
        
        mC = new MuestraC();
    }
    
    public LazyDatosMuestra(Date desde, Date hasta, Double minVal, Double maxVal, Integer tipo, boolean nuevaCreacion){
        this(desde, hasta, minVal, maxVal, tipo, nuevaCreacion, false);
        
    } 
    
    @Override
    public Muestra getRowData(String rowKey) {
        for(Muestra muestra : datasource) {
            if(muestra.getId().toString().equals(rowKey))
                return muestra;
        }
 
        return null;
    }
 
    @Override
    public Object getRowKey(Muestra muestra) {
        return muestra.getId();
    }
    
    @Override
    public List<Muestra> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters) {
        
        if(mC == null){
            mC = new MuestraC();
        }
        
        if(vacio){
            datasource = new ArrayList<>();
            this.setRowCount(0);
        }else{
            datasource = mC.findLazy(first, pageSize, desde, hasta, minVal, maxVal, tipo, creacion);
            if(datasource == null){
                setRowCount(0);
            }else{
                setRowCount(mC.findLazyCount(desde, hasta, minVal, maxVal, tipo, creacion).intValue());
            }
            
            if(this.getRowCount() <= 0 && !nuevaCreacion){
                Popup.info("Sin resultados","No se han encontrado muestras que cumplan los parametros especificados.");
            }
        }
        
        return datasource;
    }
    
    public Long numeroResultados(){
        return mC.findLazyCount(desde, hasta, minVal, maxVal, tipo, creacion);
    }
    
    public List<Muestra> todosResultados(){
        return mC.findLazy(-1, -1, desde, hasta, minVal, maxVal, tipo, creacion);
    }
}