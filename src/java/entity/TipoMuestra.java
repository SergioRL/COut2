/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author SONY
 */
@Entity
@Table(name = "tipo_muestra")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TipoMuestra.findAll", query = "SELECT t FROM TipoMuestra t"),
    @NamedQuery(name = "TipoMuestra.findById", query = "SELECT t FROM TipoMuestra t WHERE t.id = :id"),
    @NamedQuery(name = "TipoMuestra.findByDescripcion", query = "SELECT t FROM TipoMuestra t WHERE t.descripcion = :descripcion"),
    @NamedQuery(name = "TipoMuestra.findByMagnitud", query = "SELECT t FROM TipoMuestra t WHERE t.magnitud = :magnitud")})
public class TipoMuestra implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 150)
    @Column(name = "descripcion")
    private String descripcion;
    @Size(max = 45)
    @Column(name = "magnitud")
    private String magnitud;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "tipo")
    private List<Muestra> muestraList;

    public TipoMuestra() {
    }

    public TipoMuestra(Integer id) {
        this.id = id;
    }

    public TipoMuestra(Integer id, String descripcion) {
        this.id = id;
        this.descripcion = descripcion;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getMagnitud() {
        return magnitud;
    }

    public void setMagnitud(String magnitud) {
        this.magnitud = magnitud;
    }

    @XmlTransient
    public List<Muestra> getMuestraList() {
        return muestraList;
    }

    public void setMuestraList(List<Muestra> muestraList) {
        this.muestraList = muestraList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TipoMuestra)) {
            return false;
        }
        TipoMuestra other = (TipoMuestra) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.TipoMuestra[ id=" + id + " ]";
    }
    
}
