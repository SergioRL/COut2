/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package raspnWS;

import common.Constantes;
import common.Utilidades;
import controler.MuestraC;
import entity.Muestra;
import java.math.BigDecimal;
import java.util.Date;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author SONY
 */
public class MuestraTask extends TimerTask{
    
    private double valor;
    private final MuestraC mC;
    
    public MuestraTask(){
        super();
        mC = new MuestraC();
    }

    @Override
    public void run() {
        System.out.println("COMIENZO");
        try {
            valor = ServiciosRestful.getMuestraCO2();
            Muestra m = Utilidades.crearNuevaMuestra(Constantes.ID_NIVEL_CO2, new Date(), new BigDecimal(valor));
            mC.merge(m);
        } catch (Exception ex) {
            Logger.getLogger(MuestraTask.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("Valor Actual: "+valor+" .");
    }

}
