/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package raspnWS;

import common.Constantes.*;

/**
 *
 * @author SeRRi
 */
public class EstadoSistema {
    private EstadoAutomatico estadoAutomatico;
    private EstadoActuador estadoActuador;

    public EstadoSistema() {
        this.estadoAutomatico = EstadoAutomatico.DESCONOCIDO;
        this.estadoActuador = EstadoActuador.DESCONOCIDO;
    }
    
    public EstadoSistema(EstadoAutomatico estadoAutomatico, EstadoActuador estadoActuador) {
        this.estadoAutomatico = EstadoAutomatico.DESCONOCIDO;
        this.estadoActuador = estadoActuador;
    }

    public EstadoAutomatico getEstadoAutomatico() {
        return estadoAutomatico;
    }

    public void setEstadoAutomatico(EstadoAutomatico estadoAutomatico) {
        this.estadoAutomatico = estadoAutomatico;
    }

    public EstadoActuador getEstadoActuador() {
        return estadoActuador;
    }

    public void setEstadoActuador(EstadoActuador estadoActuador) {
        this.estadoActuador = estadoActuador;
    }
    
}
