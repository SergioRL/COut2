/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package raspnWS;

/**
 *
 * @author SeRRi
 */
public class ConstantesWS {
    
    // APLICACIONES
    
    public static final String APLICACION_RASPN = "/apiRest";
    
    // SERVICIOS
    
    public static final String SERVICIO_CONTROLES = "/controles";
    
    //METODOS
    
    public static final String GET_ESTADO_SISTEMA = "/getEstadoActual";
    
    public static final String GET_ENCENDER_AUTOMATICO = "/activarAutomatico";
    
    public static final String GET_APAGAR_AUTOMATICO = "/desactivarAutomatico";
    
    public static final String GET_ABRIR_ACTUADOR = "/activarActuador";
    
    public static final String GET_CERRAR_ACTUADOR = "/desactivarActuador";
    
    public static final String GET_NIVEL_CO2_ACTUAL = "/getMuestra";
    
    public static final String GET_GET_NIVEL_MAX_CO2 = "/getNivelMax"; 
    
    public static final String POST_SET_NIVEL_MAX_CO2 = "/setNivelMax"; 
    
    //KEYS JSON MUESTRAS
    
    public static final String JSON_MUESTRA_NIVEL_CO2 = "1.1";
    
    //KEYS JSON
    
    public static final String JSON_ESTADO_AUTOMATICO = "2";
    
    public static final String JSON_ESTADO_ACTUADOR = "3";
    
    public static final String JSON_MAX_NIVEL_CO2 = "4";
    
    //PARAMS REQUEST
    
    public static final String PARAM_MAX_NIVEL_CO2 = "nivelMax";
}
