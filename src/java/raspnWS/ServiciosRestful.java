/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package raspnWS;

import beans.AppWSBean;
import common.Constantes;
import common.Utilidades;
import controler.VariableC;
import entity.Variable;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.primefaces.json.JSONException;
import org.primefaces.json.JSONObject;

/**
 *
 * @author SeRRi
 */
public class ServiciosRestful {
    
    private enum MetodoRequest {GET,POST};
    
    public static EstadoSistema setNivelMaxCO2(double maxCO2){
        EstadoSistema result = new EstadoSistema(Constantes.EstadoAutomatico.DESCONOCIDO, Constantes.EstadoActuador.DESCONOCIDO);
        
        try{
            result = interpretarEstadoSistema(llamadaServicio(ConstantesWS.SERVICIO_CONTROLES,ConstantesWS.POST_SET_NIVEL_MAX_CO2+"/"+maxCO2,MetodoRequest.POST, null));
        }catch(Exception e){
            Logger.getLogger(ServiciosRestful.class.getName()).log(Level.SEVERE, null, e);
        }
        
        return result;
    }
    
    public static EstadoSistema getEstadoSistema(){
        EstadoSistema result = new EstadoSistema(Constantes.EstadoAutomatico.DESCONOCIDO, Constantes.EstadoActuador.DESCONOCIDO);
        
        try{
            result = interpretarEstadoSistema(llamadaServicio(ConstantesWS.SERVICIO_CONTROLES,ConstantesWS.GET_ESTADO_SISTEMA,MetodoRequest.GET, null));
        }catch(Exception e){
            Logger.getLogger(ServiciosRestful.class.getName()).log(Level.SEVERE, null, e);
        }
        
        return result;
    }
    
    public static Double getMuestraCO2(){
        Double result = null;
        
        try{
            String resultadoLlamada = llamadaServicio(ConstantesWS.SERVICIO_CONTROLES,ConstantesWS.GET_NIVEL_CO2_ACTUAL,MetodoRequest.GET, null);
            
            JSONObject jsonRespuesta = new JSONObject(resultadoLlamada);
            
            result = jsonRespuesta.getDouble(ConstantesWS.JSON_MUESTRA_NIVEL_CO2);
            
        }catch(Exception e){
            Logger.getLogger(ServiciosRestful.class.getName()).log(Level.SEVERE, null, e);
        }
        
        return result;
    }
    
    public static Double getMaxCO2(){
        Double result = null;
        
        try{
            String resultadoLlamada = llamadaServicio(ConstantesWS.SERVICIO_CONTROLES,ConstantesWS.GET_GET_NIVEL_MAX_CO2,MetodoRequest.GET, null);
            
            JSONObject jsonRespuesta = new JSONObject(resultadoLlamada);
            
            result = jsonRespuesta.getDouble(ConstantesWS.JSON_MAX_NIVEL_CO2);
            
        }catch(Exception e){
            Logger.getLogger(ServiciosRestful.class.getName()).log(Level.SEVERE, null, e);
        }
        
        return result;
    }
    
    public static EstadoSistema activarAutomatico(){
        EstadoSistema result = new EstadoSistema(Constantes.EstadoAutomatico.DESCONOCIDO, Constantes.EstadoActuador.DESCONOCIDO);
        
        try{
            result = interpretarEstadoSistema(llamadaServicio(ConstantesWS.SERVICIO_CONTROLES,ConstantesWS.GET_ENCENDER_AUTOMATICO,MetodoRequest.GET, null));
        }catch(Exception e){
            Logger.getLogger(ServiciosRestful.class.getName()).log(Level.SEVERE, null, e);
        }
        
        return result;
    }
    
    public static EstadoSistema desactivarAutomatico(){
        EstadoSistema result = new EstadoSistema(Constantes.EstadoAutomatico.DESCONOCIDO, Constantes.EstadoActuador.DESCONOCIDO);
        
        try{
            result = interpretarEstadoSistema(llamadaServicio(ConstantesWS.SERVICIO_CONTROLES,ConstantesWS.GET_APAGAR_AUTOMATICO,MetodoRequest.GET, null));
        }catch(Exception e){
            Logger.getLogger(ServiciosRestful.class.getName()).log(Level.SEVERE, null, e);
        }
        
        return result;
    }
    
    public static EstadoSistema abrirActuador(){
        EstadoSistema result = new EstadoSistema(Constantes.EstadoAutomatico.DESCONOCIDO, Constantes.EstadoActuador.DESCONOCIDO);
        
        try{
            result = interpretarEstadoSistema(llamadaServicio(ConstantesWS.SERVICIO_CONTROLES,ConstantesWS.GET_ABRIR_ACTUADOR,MetodoRequest.GET, null));
        }catch(Exception e){
            Logger.getLogger(ServiciosRestful.class.getName()).log(Level.SEVERE, null, e);
        }
        
        return result;
    }
    
    public static EstadoSistema cerrarActuador(){
        EstadoSistema result = new EstadoSistema(Constantes.EstadoAutomatico.DESCONOCIDO, Constantes.EstadoActuador.DESCONOCIDO);
        
        try{
            result = interpretarEstadoSistema(llamadaServicio(ConstantesWS.SERVICIO_CONTROLES,ConstantesWS.GET_CERRAR_ACTUADOR,MetodoRequest.GET, null));
        }catch(Exception e){
            Logger.getLogger(ServiciosRestful.class.getName()).log(Level.SEVERE, null, e);
        }
        
        return result;
    }
    
    private static String llamadaServicio(String servicio, String metodo, MetodoRequest metodoRequest, Map<String,Object> parametros){
        String result = null;
        
        try{
            Object auxBean = Utilidades.buscarBean("appWSBean");
            AppWSBean appWSBean = auxBean == null ? null : (AppWSBean) auxBean;
            Variable urlVar = appWSBean == null ? null : appWSBean.getUrlRaspberry();

            if(urlVar == null){
                urlVar = new VariableC().findVariableByNombre(Constantes.VARIABLE_URL_RASPBERRY);
            }

            String urlString = (urlVar == null ? Constantes.POR_DEFECTO_URL_RASPBERRY : urlVar.getValor())+ConstantesWS.APLICACION_RASPN+servicio+metodo;
            if(metodoRequest.equals(MetodoRequest.GET) && parametros != null){
                urlString += "?"+parametrosGet(parametros);
            }

            URL url = new URL(urlString);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod(metodoRequest.toString());
        
            try(BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()))){
                String inputLine;
                StringBuffer content = new StringBuffer();
                while ((inputLine = in.readLine()) != null) {
                    content.append(inputLine);
                }
                in.close();
                result = content.toString();
            }catch(Exception e){
                Logger.getLogger(ServiciosRestful.class.getName()).log(Level.SEVERE, "ERROR: Error al leer la respuesta.", e);
            }
            
        }catch(MalformedURLException e){
            Logger.getLogger(ServiciosRestful.class.getName()).log(Level.SEVERE, "ERROR: URL con formato incorrecto.", e);
        }catch(ProtocolException e){
            Logger.getLogger(ServiciosRestful.class.getName()).log(Level.SEVERE, "ERROR: Error durante la llamada HTTP.", e);
        }catch(IOException e){
            Logger.getLogger(ServiciosRestful.class.getName()).log(Level.SEVERE, "ERROR: Error de entrada/salida.", e);
        }catch(JSONException e){
            Logger.getLogger(ServiciosRestful.class.getName()).log(Level.SEVERE, "ERROR: JSON con formato incorrecto.", e);
        }
        
        return result;
    }
    
    private static String parametrosGet(Map<String,Object> parametros) throws UnsupportedEncodingException{
        StringBuilder strBuilder = new StringBuilder();
        
        for(String key : parametros.keySet()){
            strBuilder.append("&");
            strBuilder.append(URLEncoder.encode(key,"UTF-8"));
            strBuilder.append("=");
            strBuilder.append(URLEncoder.encode(parametros.get(key).toString(),"UTF-8"));
        }
        
        return Utilidades.estaVacio(strBuilder.toString()) ? "" : strBuilder.substring(1);
    }
    
    private static EstadoSistema interpretarEstadoSistema(String cadenaJson){
         EstadoSistema result = new EstadoSistema(Constantes.EstadoAutomatico.DESCONOCIDO, Constantes.EstadoActuador.DESCONOCIDO);
        
        try{
            JSONObject jsonRespuesta = new JSONObject(cadenaJson);
            String estadoAutomatico,estadoActuador;
            
            estadoAutomatico = jsonRespuesta.get(ConstantesWS.JSON_ESTADO_AUTOMATICO).toString();
            result.setEstadoAutomatico(estadoAutomatico == null ? Constantes.EstadoAutomatico.DESCONOCIDO
                    : Boolean.parseBoolean(estadoAutomatico) ? Constantes.EstadoAutomatico.ENCENDIDO : Constantes.EstadoAutomatico.APAGADO);
            
            estadoActuador = jsonRespuesta.get(ConstantesWS.JSON_ESTADO_ACTUADOR).toString();
            result.setEstadoActuador(estadoActuador == null ? Constantes.EstadoActuador.DESCONOCIDO
                    : Boolean.parseBoolean(estadoActuador) ? Constantes.EstadoActuador.ABIERTO : Constantes.EstadoActuador.CERRADO);
            
        }catch(JSONException e){
            Logger.getLogger(ServiciosRestful.class.getName()).log(Level.SEVERE, "ERROR: Error al parsear JSON.", e);
        }catch(Exception e){
            Logger.getLogger(ServiciosRestful.class.getName()).log(Level.SEVERE, null, e);
        }
        
        return result;
    }
}
