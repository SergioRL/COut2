/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filtro;

import beans.SessionBean;
import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author SONY
 */
public class FiltroLogin implements Filter{
    
    private SessionBean sb;
    private FilterConfig config;
    private String appContext;
    
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        config = filterConfig;
    }
    
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        SessionBean loginBean = (SessionBean)((HttpServletRequest)request).getSession().getAttribute("sessionBean");
        
        if(!((HttpServletResponse)response).isCommitted()){
            if (loginBean != null && loginBean.comprobarLogin()) {
                String contextPath = ((HttpServletRequest)request).getContextPath();
                ((HttpServletResponse)response).sendRedirect(contextPath + "/panelControl/panelPrincipal.xhtml");
            }
        }
        
        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {
       config = null;
    }
    
}
