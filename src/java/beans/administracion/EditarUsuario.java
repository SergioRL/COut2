/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans.administracion;

import beans.SessionBean;
import com.lambdaworks.crypto.SCryptUtil;
import common.Constantes;
import common.Popup;
import common.Utilidades;
import controler.AccionC;
import controler.PerfilC;
import controler.UsuarioC;
import entity.Accion;
import entity.Perfil;
import entity.Usuario;
import java.io.Serializable;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;
import org.kohsuke.rngom.util.Utf16;

/**
 *
 * @author SONY
 */
@ManagedBean
@ViewScoped
public class EditarUsuario implements Serializable{
    private Usuario usuario;
    private String identificacion,password;
    private Integer idPerfil;
    private List<SelectItem> listaPerfiles;

    private PerfilC pC;
    private UsuarioC uC;
    
    public EditarUsuario(){
        pC = new PerfilC();
        uC = new UsuarioC();
        listaPerfiles = pC.list2SelectItems(pC.getAllNoBloqueado());
        
        identificacion = null;
        password = null;
        idPerfil = null;
    }
    
    public EditarUsuario(Usuario us){
        this();
        
        if(us == null){
            Popup.cerrarPopupTemplate();
            Popup.error("Usuario no encontrado","El usuario indicado no existe.");
        }else{
            usuario = us;
            identificacion = us.getIdentificacion();
            password = null;
            idPerfil = usuario.getPerfil().getId();
        }
    }
    
    public EditarUsuario(int idUsuario){
        this(new UsuarioC().getUsuarioById(idUsuario));
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
        
        identificacion = usuario.getIdentificacion();
        password = null;
        idPerfil = usuario.getPerfil().getId();
    }
    
    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getIdPerfil() {
        return idPerfil;
    }

    public void setIdPerfil(Integer idPerfil) {
        this.idPerfil = idPerfil;
    }

    public List<SelectItem> getListaPerfiles() {
        return listaPerfiles;
    }

    public void setListaPerfiles(List<SelectItem> listaPerfiles) {
        this.listaPerfiles = listaPerfiles;
    }
    
    public void cancelar(){
        Popup.cerrarPopupTemplate();
    }
    
    public void guardar(){
        if(usuario == null){
            Popup.cerrarPopupTemplate();
            Popup.error("Error al editar", "No se ha podido editar el usuario. No se pudo recuperar el usuario a editar.");
        }else{
            Pattern pattern = Pattern.compile("[0-9a-zA-Z_-]{1,40}");
            Matcher matcher = pattern.matcher(identificacion);
            
            if(matcher.matches()){
                UsuarioC uC = new UsuarioC();
                
                if(!usuario.getIdentificacion().equals(identificacion) && uC.existeUsuarioIdentificacion(identificacion)){
                    identificacion = null;
                    Popup.error("Error al editar", "El nuevo nombre elegido ya está en uso. Eliga otro por favor.");
                }else{
                    Popup.cerrarPopupTemplate();
                    try{
                        SessionBean sb = (SessionBean) Utilidades.buscarBean("sessionBean");
                        Perfil perf = pC.getPerfilById(idPerfil);
                        
                        String descAccion1 ="";
                        String descAccion2 ="";
                        
                        if(!identificacion.equals(usuario.getIdentificacion()))
                            descAccion1 += "Nueva identificacion: "+identificacion+" ";
                        usuario.setIdentificacion(identificacion);
                        if(!Utilidades.estaVacio(password)){
                            usuario.setPassword(SCryptUtil.scrypt(password,16,16,16));
                            descAccion1 += "Nueva contraseña.";
                        }
                        if(usuario.getPerfil().getId() != idPerfil){
                            descAccion2 += "Editado perfil usuario "+usuario.getIdentificacion()+" (ID: "+usuario.getId()+"). Pasa de ser "+usuario.getPerfil().getNombre()+" a ser "+perf.getNombre();
                        }
                        usuario.setPerfil(perf);
                        
                        
                        
                        
                        
                        usuario = uC.merge(usuario);
                        
                        if(usuario == null){
                            throw new RuntimeException("Ha ocurrido algún error al introducir en base de datos.");
                        }else{
                            if(sb.getUsuarioConectado().getId().equals(usuario.getId())){
                                sb.setUsuarioConectado(usuario);
                            }
                                     
                            
                            if(!Utilidades.estaVacio(descAccion1)){
                                descAccion1 = "Editado usuario "+usuario.getIdentificacion()+"(ID: "+usuario.getId()+")."+ descAccion1;
                                Accion a1 = Utilidades.crearNuevaAccionActual(Constantes.ID_CAMBIO_CREDENCIALES_USUARIO,descAccion1, sb.getUsuarioConectado());
                                new AccionC().merge(a1);
                            }
                            
                            if(!Utilidades.estaVacio(descAccion2)){
                                Accion a2 = Utilidades.crearNuevaAccionActual(Constantes.ID_CAMBIO_PERFIL_USUARIO,descAccion2, sb.getUsuarioConectado());
                                new AccionC().merge(a2);
                            }
                        }
                        
                        Popup.success("Usuario Editado", "Se ha editado correctamente al usuario."); 
                    }catch(Exception e){
                        e.printStackTrace();
                        Popup.error("Error Edición", "Ha ocurrido algún error durante la edición del usuario. Consulte la consola del sistema para más información."); 
                    }
                    
                    identificacion = null;
                    password = null;
                }
            }else{
                Popup.error("Error al editar","El identificador solo puede contener carácteres alfanuméricos así como guión/guión bajo y además no puede tener mas de 40.");
            }
        }
    }
    
}
