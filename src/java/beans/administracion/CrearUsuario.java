/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans.administracion;

import beans.SessionBean;
import com.lambdaworks.crypto.SCryptUtil;
import common.Constantes;
import common.Popup;
import common.Utilidades;
import controler.AccionC;
import controler.PerfilC;
import controler.TipoAccionC;
import controler.UsuarioC;
import entity.Accion;
import entity.Perfil;
import entity.Usuario;
import java.io.Serializable;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;
import org.primefaces.context.RequestContext;

/**
 *
 * @author SONY
 */
@ManagedBean
@ViewScoped
public class CrearUsuario implements Serializable{
    private String identificacion;
    private String password;
    private Integer perfil;
    
    private String mensajeError;
    
    private List<SelectItem> listaPerfiles;
    
    private PerfilC pC;
    private AccionC aC;
    private TipoAccionC taC;
    
    public CrearUsuario(){
        pC = new PerfilC();
        aC = new AccionC();
        taC = new TipoAccionC();
        listaPerfiles = pC.list2SelectItems(pC.getAllPerfil());
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getPerfil() {
        return perfil;
    }

    public void setPerfil(Integer perfil) {
        this.perfil = perfil;
    }

    public List<SelectItem> getListaPerfiles() {
        return listaPerfiles;
    }

    public void setListaPerfiles(List<SelectItem> listaPerfiles) {
        this.listaPerfiles = listaPerfiles;
    }

    public String getMensajeError() {
        return mensajeError;
    }

    public void setMensajeError(String mensajeError) {
        this.mensajeError = mensajeError;
    }
    
    public void cancelar(){
        Popup.cerrarPopupTemplate();
    }
    
    public void guardar(){
        if(Utilidades.estaVacio(identificacion) || Utilidades.estaVacio(password) || perfil == null){
            mensajeError = "Error: Todos los campos deben tener algún valor.";
        }else{
            Pattern pattern = Pattern.compile("[0-9a-zA-Z_-]{1,40}");
            Matcher matcher = pattern.matcher(identificacion);
            
            if(matcher.matches()){
                UsuarioC uC = new UsuarioC();
                
                if(uC.existeUsuarioIdentificacion(identificacion)){
                    identificacion = null;
                    mensajeError = "Error: Ya existe un usuario con esa identificacion.";
                }else{
                    Popup.cerrarPopupTemplate();
                    try{
                        SessionBean sb = (SessionBean) Utilidades.buscarBean("sessionBean");
                        
                        Usuario nuevoUsuario = new Usuario();
                        Perfil perf = pC.getPerfilById(perfil);
                        
                        nuevoUsuario.setIdentificacion(identificacion);
                        nuevoUsuario.setPassword(SCryptUtil.scrypt(password,16,16,16));
                        nuevoUsuario.setPerfil(perf);

                        nuevoUsuario = uC.merge(nuevoUsuario);
                        if(nuevoUsuario == null){
                            throw new RuntimeException("No se ha introducido el nuevo usuario en la base de datos.");
                        }else{
                            Accion a = Utilidades.crearNuevaAccionActual(Constantes.ID_CREACION_USUARIO, "Creado nuevo usuario "+nuevoUsuario.getIdentificacion()+" con perfil "+nuevoUsuario.getPerfil().getNombre()+" e ID "+nuevoUsuario.getId()+".", sb.getUsuarioConectado());
                            new AccionC().merge(a);
                        }
                        
                        Popup.success("Usuario Creado", "El nuevo usuario se ha creado correctamente."); 
                    }catch(Exception e){
                        e.printStackTrace();
                        Popup.error("Error Creación", "Ha ocurrido algún error durante la creación del nuevo usuario. Consulte la consola del sistema para más información."); 
                    }
                    
                    mensajeError = null;
                    identificacion = null;
                    password = null;
                }
            }else{
                mensajeError = "Error: El identificador solo puede contener carácteres alfanuméricos así como guión/guión bajo y además no puede tener mas de 40.";
            }
            
        }
    }
}
