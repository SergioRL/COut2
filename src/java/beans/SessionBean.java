/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import com.lambdaworks.crypto.SCryptUtil;
import common.Constantes;
import common.Popup;
import controler.UsuarioC;
import entity.Usuario;
import java.io.IOException;
import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.servlet.FilterConfig;
import java.security.MessageDigest;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PreDestroy;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;


/**
 *
 * @author SONY
 */

@ManagedBean
@SessionScoped
public class SessionBean implements Serializable{
    
    private Usuario usuarioConectado;
    
    private String identificacion;
    private String password;
    
    private UsuarioC uC;
    
    public SessionBean(){
        uC = new UsuarioC();
    }

    public Usuario getUsuarioConectado() {
        return usuarioConectado;
    }

    public void setUsuarioConectado(Usuario usuarioConectado) {
        this.usuarioConectado = usuarioConectado;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    
    public void hacerLogin(){
        
        try{
            Usuario usuarioLogin = uC.getUsuarioByIdentificacion(identificacion);

            boolean denegar = false;

            if(usuarioLogin == null ||!SCryptUtil.check(password, usuarioLogin.getPassword())){
                denegar = true;
                Popup.error("Credenciales Incorrectas", "El usuario o contraseña introducidos son erróneos.");
            }else if(Constantes.ID_BLOQUEADO == usuarioLogin.getPerfil().getId()){
                denegar = true;
                Popup.error("Error Login", "El usuario con el que intenta acceder esta BLOQUEADO y no se le permite el acceso.");
            }

            if(denegar){
                identificacion = null;
                password = null;
            }else{
                usuarioConectado = usuarioLogin;
                try {
                    FacesContext.getCurrentInstance().getExternalContext().redirect("panelControl/panelPrincipal.xhtml?faces-redirect=true");
                } catch (IOException ex) {
                    Logger.getLogger(SessionBean.class.getName()).log(Level.SEVERE, null, ex);
                    Popup.error("Error", "Ha ocurrido algún error durante la operacion de identificacion. Por favor inténtelo más tarde.");
                }
            }

            password = null;
        }catch(Exception e){
            Popup.error("Error", "Ha ocurrido algún error durante la operacion de identificacion. Por favor inténtelo más tarde.");
        }
    } 
    
    public String hacerLogout(){
        usuarioConectado = null;
        
        return "/login.xhtml?faces-redirect=true";
    } 
    
    public boolean comprobarLogin(){
        return usuarioConectado != null;
    }
    
    public String irAdministración(){
        return "panelAdministracion.xhtml?faces-redirect=true";
    }
    
    public boolean renderAdministracion(){
        return true;
    }
    
    public Integer idAdministrador(){
        return Constantes.ID_ADMINISTRADOR;
    }
    
    @PreDestroy
    private void alAcabar(){
        uC.finalize();
        usuarioConectado = null;
    }
}
