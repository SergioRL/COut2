/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import beans.administracion.EditarUsuario;
import common.Constantes;
import common.Popup;
import common.Utilidades;
import controler.AccionC;
import controler.PerfilC;
import controler.UsuarioC;
import entity.Accion;
import entity.Perfil;
import entity.Usuario;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;
import org.primefaces.context.RequestContext;

/**
 *
 * @author SONY
 */
@ManagedBean
@ViewScoped
public class Administracion implements Serializable{
    private static final Integer ID_ADMINISTRADOR = Constantes.ID_ADMINISTRADOR;
    private static final Integer ID_BLOQUEADO = Constantes.ID_BLOQUEADO;
    
    private static final String URL_NUEVO_USUARIO = "/panelControl/popup/nuevoUsuario.xhtml";
    private static final String URL_EDITAR_USUARIO = "/panelControl/popup/editarUsuario.xhtml";
    
    private final AppWSBean appBean;
    
    private String contenidoPopup;
    private String textoCabecera;
    
    private List<Usuario> listaUsuarios;
    private List<Perfil> listaPerfiles;
    private int intervaloMuestra;
    private String urlRaspberry;
    
    private UsuarioC uC;
    private PerfilC pC;
    
    private SessionBean sb;
    
    public Administracion(){
        uC = new UsuarioC();
        pC = new PerfilC();
        
        appBean = (AppWSBean) Utilidades.buscarBean("appWSBean");
        sb = (SessionBean) Utilidades.buscarBean("sessionBean");
        
        listaUsuarios = uC.getAllUsuario();
        listaPerfiles = pC.getAllNoBloqueado();
        
    }
    
    @PostConstruct
    private void cargarOpciones(){
        if(appBean != null){
            String inMuest = appBean.getIntervaloMuestra().getValor();
            if(inMuest == null){
                intervaloMuestra = Integer.parseInt(Constantes.POR_DEFECTO_INTERVALO_MUESTRA);
            }else{
                try{
                    intervaloMuestra = Integer.parseInt(inMuest);
                }catch(Exception e){
                    e.printStackTrace();
                }
            }

            urlRaspberry = appBean.getUrlRaspberry().getValor();
        }else{
            intervaloMuestra = Integer.parseInt(Constantes.POR_DEFECTO_INTERVALO_MUESTRA);
            urlRaspberry =  Constantes.POR_DEFECTO_URL_RASPBERRY;
        }
    }

    public List<Usuario> getListaUsuarios() {
        return listaUsuarios;
    }

    public void setListaUsuarios(List<Usuario> listaUsuarios) {
        this.listaUsuarios = listaUsuarios;
    }

    public List<Perfil> getListaPerfiles() {
        return listaPerfiles;
    }

    public void setListaPerfiles(List<Perfil> listaPerfiles) {
        this.listaPerfiles = listaPerfiles;
    }

    public String getContenidoPopup() {
        return contenidoPopup;
    }

    public void setContenidoPopup(String contenidoPopup) {
        this.contenidoPopup = contenidoPopup;
    }
    
    public static Integer getID_ADMINISTRADOR() {
        return ID_ADMINISTRADOR;
    }

    public static Integer getID_BLOQUEADO() {
        return ID_BLOQUEADO;
    }
    
    public static String getURL_NUEVO_USUARIO() {
        return URL_NUEVO_USUARIO;
    }

    public static String getURL_EDITAR_USUARIO() {
        return URL_EDITAR_USUARIO;
    }

    public String getTextoCabecera() {
        return textoCabecera;
    }

    public void setTextoCabecera(String textoCabecera) {
        this.textoCabecera = textoCabecera;
    }

    public String getUrlRaspberry() {
        return urlRaspberry;
    }

    public void setUrlRaspberry(String urlRaspberry) {
        this.urlRaspberry = urlRaspberry;
    }

    public int getIntervaloMuestra() {
        return intervaloMuestra;
    }

    public void setIntervaloMuestra(int intervaloMuestra) {
        this.intervaloMuestra = intervaloMuestra;
    }
    
    
    public String crearNuevoUsuario(){
        contenidoPopup = URL_NUEVO_USUARIO;
        textoCabecera = "Nuevo Usuario";
        RequestContext.getCurrentInstance().execute("PF('popupTemplate').show()");
        return null;
    }
    
    public String editarUsuario(Usuario usuario){
        if(Constantes.ID_BLOQUEADO == usuario.getPerfil().getId().intValue()){
            Popup.error("Error","El usuario está bloqueado, por lo que no se puede editar.");
        }else{
            FacesContext.getCurrentInstance().getApplication().getELResolver().setValue(FacesContext.getCurrentInstance().getELContext(), null, "editarUsuario", new EditarUsuario(usuario));
        
            contenidoPopup = URL_EDITAR_USUARIO;
            textoCabecera = "Editar Usuario";
            RequestContext.getCurrentInstance().execute("PF('popupTemplate').show()");
        }
        
        return null;
    }
    
    public void recargarUsuarios(){
        listaUsuarios = uC.getAllUsuario();
    }
    
    public String bloquearUsuario(Usuario usuario){
        Perfil p = pC.getPerfilById(Constantes.ID_BLOQUEADO);
        usuario.setPerfil(p);
        usuario = uC.merge(usuario);
        
        if(usuario == null){
            Popup.error("Error Operacion", "Ha habido un error durante la operacion de bloqueo.");
        }else{
            SessionBean sb = (SessionBean) Utilidades.buscarBean("sessionBean");
            Accion a = Utilidades.crearNuevaAccionActual(Constantes.ID_BLOQUEO_USUARIO, "Se ha BLOQUEADO al usuario "+usuario.getIdentificacion()+" con ID "+usuario.getId()+".", sb.getUsuarioConectado());
            Popup.success("Usuario Bloqueado", "El usuario "+usuario.getIdentificacion()+" ha sido bloqueado.");
        }
        
        return null;
    }
    
    public String desbloquearUsuario(Usuario usuario){
        Perfil p = pC.getPerfilById(Constantes.ID_CONSULTOR);
        usuario.setPerfil(p);
        usuario = uC.merge(usuario);
        
        if(usuario == null){
            Popup.error("Error Operacion", "Ha habido un error durante la operacion de desbloqueo.");
        }else{
            SessionBean sb = (SessionBean) Utilidades.buscarBean("sessionBean");
            Accion a = Utilidades.crearNuevaAccionActual(Constantes.ID_DESBLOQUEO_USUARIO, "Se ha DESBLOQUEADO al usuario "+usuario.getIdentificacion()+" con ID "+usuario.getId()+".", sb.getUsuarioConectado());
            Popup.success("Usuario Desbloqueado", "El usuario "+usuario.getIdentificacion()+" ha sido desbloqueado y ahora es "+p.getNombre()+".");
        }
        
        return null;
    }
    
    public String guardarCambiosOpciones(){
        if(appBean != null){
            String mensajeError = "";
            
            AccionC accionControlador = new AccionC();

            if(appBean.getIntervaloMuestra().getValor() == null || (appBean.getIntervaloMuestra().getValor() != null && !appBean.getIntervaloMuestra().getValor().equals(String.valueOf(intervaloMuestra)))){
                if(!appBean.cambiarIntervaloMuestra(intervaloMuestra)){
                    mensajeError = "-Error al cambiar el intervalo.\n";
                }else{
                    Accion a = Utilidades.crearNuevaAccionActual(Constantes.ID_CAMBIO_TIEMPO_REPORTE, "Se ha cambiado el intervalor entre muestras a "+intervaloMuestra+" segundos.", sb.getUsuarioConectado());
                    accionControlador.merge(a);
                }
            }
            
            if(appBean.getUrlRaspberry().getValor() == null || (appBean.getUrlRaspberry().getValor() != null && !appBean.getUrlRaspberry().getValor().equals(urlRaspberry))){
                if(!appBean.cambiarUrlRaspberry(urlRaspberry)){
                    mensajeError = "-Error al cambiar la URL.\n";
                }else{
                    Accion a = Utilidades.crearNuevaAccionActual(Constantes.ID_CAMBIO_URL_RASPBERRY, "Se ha cambiado la URL del WebService a ("+urlRaspberry+").", sb.getUsuarioConectado());
                    accionControlador.merge(a);
                }
            }
            
            if(!Utilidades.estaVacio(mensajeError)){
                Popup.warning("Error al guardar", mensajeError);
            }else{
                Popup.success("Guardado correctamente", "Se han guardado todos los cambios correctamente.");
            }
        }else{
            Popup.error("Error Aplicacion", "Ha ocurrido algun erro al intentar acceder a las opciones.");
        }
        
        return null;
    }
}
