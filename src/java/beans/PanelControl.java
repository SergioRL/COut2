/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import beans.controles.ControlesCO2;
import beans.controles.ControlesEstado;
import common.Constantes;
import common.Popup;
import common.Utilidades;
import controler.AccionC;
import controler.VariableC;
import entity.Variable;
import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import sun.security.tools.KeyStoreUtil;

/**
 *
 * @author SONY
 */

@ManagedBean
@ViewScoped
public class PanelControl implements Serializable{
    
    private SessionBean sb;
    private ControlesEstado controlesEstado;
    private ControlesCO2 controlesCO2;
    
    public PanelControl(){
        sb = (SessionBean) Utilidades.buscarBean("sessionBean");
        controlesEstado = (ControlesEstado) Utilidades.buscarBean("controlesEstado");
        controlesCO2 = (ControlesCO2) Utilidades.buscarBean("controlesCO2");
    }
    
    public String ejemplo(){
        return "";
    } 
    
    public boolean renderControles(){
        boolean result = false;
        if(sb != null){
            if(sb.getUsuarioConectado() != null && sb.getUsuarioConectado().getPerfil() != null){
                result = sb.getUsuarioConectado().getPerfil().getId().equals(Constantes.ID_ADMINISTRADOR) || sb.getUsuarioConectado().getPerfil().getId().equals(Constantes.ID_OPERADOR);
            }
        }
        
        return result;
    }
    
    public void refrescarDatos(){
        controlesEstado.consultarEstadoSistema();
        controlesCO2.actualizarMaxCO2();
    }
    
}
