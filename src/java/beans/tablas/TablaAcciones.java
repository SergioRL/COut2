/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans.tablas;

import controler.AccionC;
import controler.TipoAccionC;
import controler.UsuarioC;
import entity.Accion;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;
import common.lazyModel.LazyDatosAcciones;
import org.primefaces.model.LazyDataModel;

/**
 *
 * @author SONY
 */

@ManagedBean
@ViewScoped
public class TablaAcciones implements Serializable {

    private static final int TAM_PAGINA = 10;

    private Date desde, hasta;
    private Integer usuario, tipo;
    private LazyDataModel<Accion> lazyModel;
    
    private List<SelectItem> listaUsuarios, listaTipoAcciones;
    
    private UsuarioC uC;
    private TipoAccionC taC;

    public TablaAcciones() {
        uC = new UsuarioC();
        taC = new TipoAccionC();
        
        lazyModel = new LazyDatosAcciones(desde, hasta, usuario, tipo, true);
        listaUsuarios = uC.list2SelectItems(uC.getAllUsuario());
        listaTipoAcciones = taC.list2SelectItems(taC.findAllTipoAccion());
    }

    public Date getDesde() {
        return desde;
    }

    public void setDesde(Date desde) {
        this.desde = desde;
    }

    public Date getHasta() {
        return hasta;
    }

    public void setHasta(Date hasta) {
        this.hasta = hasta;
    }

    public Integer getUsuario() {
        return usuario;
    }

    public void setUsuario(Integer usuario) {
        this.usuario = usuario;
    }

    public Integer getTipo() {
        return tipo;
    }

    public void setTipo(Integer tipo) {
        this.tipo = tipo;
    }
    
    public LazyDataModel<Accion> getLazyModel() {
        return lazyModel;
    }

    public List<SelectItem> getListaUsuarios() {
        return listaUsuarios;
    }

    public void setListaUsuarios(List<SelectItem> listaUsuarios) {
        this.listaUsuarios = listaUsuarios;
    }

    public List<SelectItem> getListaTipoAcciones() {
        return listaTipoAcciones;
    }

    public void setListaTipoAcciones(List<SelectItem> listaTipoAcciones) {
        this.listaTipoAcciones = listaTipoAcciones;
    }

    public static int getTAM_PAGINA() {
        return TAM_PAGINA;
    }

    public String buscar() {
        lazyModel = new LazyDatosAcciones(desde, hasta, usuario, tipo, false);
        return null;
    }

}
