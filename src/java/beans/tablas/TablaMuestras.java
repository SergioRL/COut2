/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans.tablas;

import beans.graficas.GraficaBean;
import common.Constantes;
import common.Popup;
import common.Utilidades;
import entity.Muestra;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import common.lazyModel.LazyDatosMuestra;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.context.RequestContext;
import org.primefaces.model.LazyDataModel;

/**
 *
 * @author SONY
 */

@ManagedBean
@ViewScoped
public class TablaMuestras implements Serializable{
    private static final int TAM_PAGINA = 10;
    private static final String URL_GRAFICA = "/panelControl/popup/graficaDatos.xhtml";
    
    private Date desde,hasta;
    private Double min,max;
    private LazyDataModel<Muestra> lazyModel;
    
    private String contenidoPopup;
    private String textoCabecera;
    
   public TablaMuestras(){
       lazyModel = new LazyDatosMuestra(desde,hasta,min,max, Constantes.ID_NIVEL_CO2, true);
   }

    public Date getDesde() {
        return desde;
    }

    public void setDesde(Date desde) {
        this.desde = desde;
    }

    public Date getHasta() {
        return hasta;
    }

    public void setHasta(Date hasta) {
        this.hasta = hasta;
    }

    public Double getMin() {
        return min;
    }

    public void setMin(Double min) {
        this.min = min;
    }

    public Double getMax() {
        return max;
    }

    public void setMax(Double max) {
        this.max = max;
    }

    public LazyDataModel<Muestra> getLazyModel() {
        return lazyModel;
    }

    public void setLazyModel(LazyDataModel<Muestra> lazyModel) {
        this.lazyModel = lazyModel;
    }
    
    public static int getTAM_PAGINA() {
        return TAM_PAGINA;
    }
    
    public String buscar(){
        lazyModel = new LazyDatosMuestra(desde, hasta, min, max, Constantes.ID_NIVEL_CO2, false);
        
        List<Muestra> muestras = ((LazyDatosMuestra)lazyModel).todosResultados();
        
        GraficaBean graficaBean = (GraficaBean) Utilidades.buscarBean("graficaBean");
        if(graficaBean == null){
            FacesContext.getCurrentInstance().getApplication().getELResolver().setValue(FacesContext.getCurrentInstance().getELContext(), null, "graficaBean", new GraficaBean(muestras));
        }else{
            graficaBean.actualizarGrafica(muestras);
        }
        
        return null;
    }

    public String getContenidoPopup() {
        return contenidoPopup;
    }

    public void setContenidoPopup(String contenidoPopup) {
        this.contenidoPopup = contenidoPopup;
    }

    public String getTextoCabecera() {
        return textoCabecera;
    }

    public void setTextoCabecera(String textoCabecera) {
        this.textoCabecera = textoCabecera;
    }
    
    public String mostrarGrafica(){
        if(((LazyDatosMuestra)lazyModel).numeroResultados().equals(new Long(0))){
            Popup.warning("Muestras Insuficientes", "No hay un número suficiente de muestras para crear una gráfica.");
        }else{
            List<Muestra> muestras = ((LazyDatosMuestra)lazyModel).todosResultados();
//            GraficaBean graficaBean = (GraficaBean) Utilidades.buscarBean("graficaBean");
//            graficaBean.setListaMuestras(muestras);
//            FacesContext.getCurrentInstance().getApplication().getELResolver().setValue(FacesContext.getCurrentInstance().getELContext(), null, "graficaBean", new GraficaBean(muestras));

            contenidoPopup = URL_GRAFICA;
            textoCabecera = null;
            RequestContext.getCurrentInstance().execute("PF('popupTemplate').show()");
        }
        
        return null;
    }
}
