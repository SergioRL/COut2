/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans.controles;

import beans.AppWSBean;
import beans.SessionBean;
import common.Constantes;
import common.Popup;
import common.Utilidades;
import controler.AccionC;
import controler.VariableC;
import entity.Variable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import raspnWS.ServiciosRestful;

/**
 *
 * @author SeRRi
 */
@ManagedBean
@ViewScoped
public class ControlesCO2 {
    private SessionBean sb;
    private AppWSBean appBean;
    private Variable maxNivelCO2;
    
    private Double maxCO2;
    private boolean cambiarMaxHabilitado;
    
    private AccionC accionControlador;
    private VariableC vC;

    public ControlesCO2() {
        sb = (SessionBean) Utilidades.buscarBean("sessionBean");
        appBean = (AppWSBean) Utilidades.buscarBean("appWSBean");
        vC = new VariableC();
        accionControlador = new AccionC();
        
        actualizarMaxCO2();
    }
    
    public String aplicarNivelCO2(){
        try {
            if(vC == null)
                vC = new VariableC();
           
            if(maxCO2 == null){
                maxCO2 = Double.parseDouble(maxNivelCO2.getValor());
                Popup.warning("Error", "El nivel máximo no puede ser menor que "+Constantes.MIN_NIVEL_MAXIMO+".");
            }else{
                if(maxCO2.compareTo(Constantes.MIN_NIVEL_MAXIMO) >= 0 ){
                    if(!setMaxNivelCO2(maxCO2)){
                        Popup.error("Error", "Ha ocurrido un error al guardar el nivel o durante la recepción de la respuesta."); 
                    }else{
                        
                        accionControlador.merge(Utilidades.crearNuevaAccionActual(Constantes.ID_CABIO_NIVEL_CO2, "Cambiado el nivel máximo de CO2: "+maxCO2+" ppm." , sb.getUsuarioConectado()));
                        maxNivelCO2 = vC.findVariableByNombre(Constantes.VARIABLE_NIVEL_MAXIMO);
                        
                        if(appBean.cambiarNivelMaximoWS(maxCO2)){
                            Popup.success("Nivel Modificado", "Se ha cambiado el nivel máximo correctamente.");
                        }else{
                            Popup.error("Error", "Ha ocurrido un error al guardar el nivel.");
                        }
                    }
                }else{
                    maxCO2 = Double.parseDouble(maxNivelCO2.getValor());
                    Popup.warning("Error", "El nivel máximo no puede ser menor que "+Constantes.MIN_NIVEL_MAXIMO+".");
                }
            }
        }catch(Exception ex){
            ex.printStackTrace();
        }
        return  null;
    }

    public Double getMaxCO2() {
        return maxCO2;
    }

    public void setMaxCO2(Double maxCO2) {
        this.maxCO2 = maxCO2;
    }

    public boolean isCambiarMaxHabilitado() {
        return cambiarMaxHabilitado;
    }
    
    public Double getNivelMaxCO2Actual(){
        return ServiciosRestful.getMaxCO2();
    }
    
    public static boolean setMaxNivelCO2(Double maxCO2){
        ControlesEstado controlesEstado = (ControlesEstado) Utilidades.buscarBean("controlesEstado");
        
        controlesEstado.setEstadoSistema(ServiciosRestful.setNivelMaxCO2(maxCO2 == null ||  maxCO2 < 0 ? 0 : maxCO2));
        
        return controlesEstado.estadoReconocido();
        
    }
    
    public final void actualizarMaxCO2(){
        
        try{
            Double valorMaxCO2 = getNivelMaxCO2Actual();

            if(valorMaxCO2 == null){
                cambiarMaxHabilitado = false;

                maxNivelCO2 = vC.findVariableByNombre(Constantes.VARIABLE_NIVEL_MAXIMO);
                if(maxNivelCO2 != null){
                    try{
                        maxCO2 = Double.valueOf(maxNivelCO2.getValor());
                    }catch(Exception e){
                        e.printStackTrace();
                        maxCO2 = null;
                    }

                }else{
                   maxNivelCO2 = new Variable();
                   maxNivelCO2.setNombre(Constantes.VARIABLE_NIVEL_MAXIMO);
                }

            }else{
                cambiarMaxHabilitado = true;
                maxCO2 = valorMaxCO2;
            }
        }catch(Exception e){
            System.err.println("Excepcion no controlada durate 'actualizarMaxCO2'");
            e.printStackTrace();
        }
        
    }
}
