/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans.controles;

import beans.SessionBean;
import raspnWS.EstadoSistema;
import raspnWS.ServiciosRestful;
import common.Constantes;
import common.Constantes.EstadoActuador;
import common.Constantes.EstadoAutomatico;
import common.Utilidades;
import controler.AccionC;
import java.io.Serializable;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author SONY
 */
@ManagedBean
@ViewScoped
public class ControlesEstado implements Serializable{
    private final SessionBean sb;
    
    private EstadoSistema estadoSistema;
    
    private final AccionC accionControlador;

    public ControlesEstado() {
        sb = (SessionBean) Utilidades.buscarBean("sessionBean");
        accionControlador = new AccionC();
        
        consultarEstadoSistema();
    }

    public EstadoSistema getEstadoSistema() {
        return estadoSistema;
    }

    public void setEstadoSistema(EstadoSistema estadoSistema) {
        this.estadoSistema = estadoSistema;
        estadoReconocido();
    }

    public EstadoAutomatico getEstadoAutomatico() {
        return estadoSistema.getEstadoAutomatico();
    }

    public EstadoActuador getEstadoActuador() {
        return estadoSistema.getEstadoActuador();
    }

    
    public boolean mostrarBtnAutomatico(){
        return !getEstadoAutomatico().equals(EstadoAutomatico.DESCONOCIDO);
    }
    
    public boolean mostrarBtnActuador(){
        return !getEstadoActuador().equals(EstadoActuador.DESCONOCIDO);
    }
    
    public boolean textoEncenderAutomatico(){
        return  getEstadoAutomatico().equals(EstadoAutomatico.APAGADO);
    }
    
    public boolean textoAbrirActuador(){
        return getEstadoActuador().equals(EstadoActuador.CERRADO);
    }
    
    public final void consultarEstadoSistema(){
        try {
            estadoSistema = ServiciosRestful.getEstadoSistema();
        } catch (Exception ex) {
            Logger.getLogger(ControlesEstado.class.getName()).log(Level.SEVERE, "ERROR: consultarEstadoSistema().", ex);
        }
    }
    
    public String cambiarEstadoAutomatico(boolean encender){
        try {
            
            if(encender){
                
                estadoSistema = ServiciosRestful.activarAutomatico();
                
                
                if(estadoReconocido()){
                    accionControlador.merge(Utilidades.crearNuevaAccionActual(Constantes.ID_ENCENDIDO_AUTOMATICO, "Sistema AUTOMATICO: ENCENDIDO.",sb.getUsuarioConectado()));
                }
            }else{
                
                estadoSistema = ServiciosRestful.desactivarAutomatico();
                
                if(estadoReconocido()){
                    accionControlador.merge(Utilidades.crearNuevaAccionActual(Constantes.ID_APAGADO_AUTOMATICO, "Sistema AUTOMATICO: APAGADO.",sb.getUsuarioConectado()));
                }
                
            }
            
        } catch (Exception ex) {
            Logger.getLogger(ControlesEstado.class.getName()).log(Level.SEVERE, "ERROR: cambiarEstadoAutomatico().", ex);
        }
        return null;
    }
    
    public String cambiarEstadoActuador(boolean abrir){
        try {
            
            if(abrir){
                
                estadoSistema = ServiciosRestful.abrirActuador();
                
                if(estadoReconocido()){
                    accionControlador.merge(Utilidades.crearNuevaAccionActual(Constantes.ID_APERTURA_MANUAL, "Sistema MANUAL: ABIERTO.",sb.getUsuarioConectado()));
                }
            }else{
                
                estadoSistema = ServiciosRestful.cerrarActuador();
                
                if(estadoReconocido()){
                    accionControlador.merge(Utilidades.crearNuevaAccionActual(Constantes.ID_CIERRE_MANUAL, "Sistema MANUAL: CERRADO.",sb.getUsuarioConectado()));
                }
            }
            
        } catch (Exception ex) {
            Logger.getLogger(ControlesEstado.class.getName()).log(Level.SEVERE, "ERROR: cambiarEstadoActuador().", ex);
        }
        
        return null;
    }
    
    boolean estadoReconocido(){
        return !(getEstadoAutomatico().equals(EstadoAutomatico.DESCONOCIDO) || getEstadoActuador().equals(EstadoActuador.DESCONOCIDO));
    }
    
}
