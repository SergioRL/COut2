/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans.graficas;

import common.Popup;
import common.Utilidades;
import controler.MuestraC;
import entity.Muestra;
import java.awt.image.RenderedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import javax.imageio.ImageIO;
import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.CategoryAxis;
import org.primefaces.model.chart.DateAxis;
import org.primefaces.model.chart.LineChartModel;
import org.primefaces.model.chart.LineChartSeries;
import org.apache.commons.codec.binary.Base64;
import org.primefaces.model.chart.LegendPlacement;

/**
 *
 * @author SONY
 */
@ManagedBean
@ViewScoped
public class GraficaBean implements Serializable{
    
    private List<Muestra> listaMuestras;
    private String txtImagen;
    private LineChartModel graficaMuestras;
    private Date primeraFecha,ultimaFecha;
    private final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public GraficaBean() {
        listaMuestras = new MuestraC().getAllMuestra();
        MuestraC mC = new MuestraC();
        
        primeraFecha = mC.getPrimeraFecha();
        ultimaFecha = mC.getUltimaFecha();
    }
    
    public GraficaBean(List<Muestra> muestras) {
        listaMuestras = muestras;
    }
    
    public GraficaBean(List<Muestra> muestras, Date fechaMenor, Date fechaMayor) {
        listaMuestras = muestras;
        primeraFecha = fechaMenor;
        ultimaFecha = fechaMayor;
    }
    
    @PostConstruct
    private void initGrafica(){
        if(listaMuestras != null && listaMuestras.size() > 0){
            graficaMuestras = initLinearModel(listaMuestras);
            graficaMuestras.setTitle("Nivel CO2");
            graficaMuestras.setLegendPosition("e");
    //        lineModel2.setShowPointLabels(true); <--- SOLO CUANDO NO SEAN DEMASIADOS PUNTOS
            Axis yAxis = graficaMuestras.getAxis(AxisType.Y);
            yAxis.setLabel("Nivel CO2 (ppm)");
            yAxis.setMin(0);
            
            DateAxis axis = new DateAxis("Fecha");
            axis.setTickCount(12);
            axis.setTickAngle(-50);
            axis.setTickFormat("%d-%m-%Y %H:%M:%S");
            
            Long rango = ultimaFecha.getTime() - primeraFecha.getTime();
            long margen = (rango/100)*5;
            Date limiteIzquierda = new Date(primeraFecha.getTime() - (margen + 1000));
            Date limiteDerecha = new Date(ultimaFecha.getTime() + (margen + 1000));
            
            axis.setMin(sdf.format(limiteIzquierda));
            axis.setMax(sdf.format(limiteDerecha));
            graficaMuestras.getAxes().put(AxisType.X, axis);
        }
    }
    
    private LineChartModel initLinearModel(List<Muestra> muestras) {
        LineChartModel model = new LineChartModel();
        model.setStacked(true);
        model.setLegendPosition("ne");
 
        LineChartSeries series1 = new LineChartSeries();
        series1.setLabel("CO2");
        series1.setShowMarker(false);
 
        for(Muestra m : muestras){
            series1.set(sdf.format(m.getFecha()), m.getValor());
            if(m.getFecha() != null){
                if(ultimaFecha == null || m.getFecha().compareTo(ultimaFecha) > 0){
                    ultimaFecha = m.getFecha();
                }
                
                if(primeraFecha == null || m.getFecha().compareTo(primeraFecha) < 0){
                    primeraFecha = m.getFecha();
                }
            }
        }
 
        model.addSeries(series1);
         
        return model;
    }

    public LineChartModel getGraficaMuestras() {
        return graficaMuestras;
    }

    public void setGraficaMuestras(LineChartModel graficaMuestras) {
        this.graficaMuestras = graficaMuestras;
    }

    public String getTxtImagen() {
        return txtImagen;
    }

    public void setTxtImagen(String txtImagen) {
        this.txtImagen = txtImagen;
    }

    public List<Muestra> getListaMuestras() {
        return listaMuestras;
    }

    public void setListaMuestras(List<Muestra> listaMuestras) {
        this.listaMuestras = listaMuestras;
    }
    
    public void exportarImagen(ActionEvent event){
        File imagen = crearImagen("chart"+System.currentTimeMillis()+".png");
        if(imagen != null){
            Utilidades.descargarFichero(imagen.getAbsolutePath(), "GraficoMuestras.png");
        }else{
            Popup.error("Error Exportar", "Han ocurrido errores durante la exportación.");
        }
    }
    
    private File crearImagen(String nombreFichero){
        File fichResult = null;
        if(txtImagen.split(",").length > 1){
            String encoded = txtImagen.split(",")[1];
            byte[] decoded = Base64.decodeBase64(encoded);
            // Write to a .png file
            try {
                RenderedImage renderedImage = ImageIO.read(new ByteArrayInputStream(decoded));
                fichResult = new File(nombreFichero);
                ImageIO.write(renderedImage, "png", fichResult); // use a proper path & file name here.
                
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return fichResult;
    }
    
    public void actualizarGrafica(List<Muestra> listaMuestras){
        primeraFecha = null;
        ultimaFecha = null;
        this.setListaMuestras(listaMuestras);
        initGrafica();
    }
}
