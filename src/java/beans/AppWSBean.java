/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import raspnWS.MuestraTask;
import common.Constantes;
import common.Utilidades;
import controler.VariableC;
import entity.Variable;
import java.util.Timer;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

/**
 *
 * @author SONY
 */
@ManagedBean (eager = true)
@ApplicationScoped
public class AppWSBean {
    
    private int segundosEspera,tiempoEspera;

    private Timer timer;
    private VariableC variableControlador;
    private Variable urlRaspberry,intervaloMuestra;
    private MuestraTask accionTimer;
    
    public AppWSBean() {
        System.out.println("INICIALIZO BEAN");
        variableControlador = new VariableC();
        
        urlRaspberry = variableControlador.findVariableByNombre(Constantes.VARIABLE_URL_RASPBERRY);
        if(urlRaspberry == null){
            urlRaspberry = new Variable();
            urlRaspberry.setNombre(Constantes.VARIABLE_URL_RASPBERRY);
            urlRaspberry.setValor(Constantes.POR_DEFECTO_URL_RASPBERRY);
            urlRaspberry = variableControlador.merge(urlRaspberry);
        }
        
        intervaloMuestra = variableControlador.findVariableByNombre(Constantes.VARIABLE_INTERVALO_MUESTRA);
        if(intervaloMuestra == null){
            intervaloMuestra = new Variable();
            intervaloMuestra.setNombre(Constantes.VARIABLE_INTERVALO_MUESTRA);
            intervaloMuestra.setValor(Constantes.POR_DEFECTO_INTERVALO_MUESTRA);
            intervaloMuestra = variableControlador.merge(intervaloMuestra);
        }
        
        Variable nivelCO2 = variableControlador.findVariableByNombre(Constantes.VARIABLE_NIVEL_MAXIMO);
        if(nivelCO2 == null){
            nivelCO2 = new Variable();
            nivelCO2.setNombre(Constantes.VARIABLE_NIVEL_MAXIMO);
            nivelCO2.setValor(Constantes.POR_DEFECTO_NIVEL_MAXIMO);
            variableControlador.merge(nivelCO2);
        }
        
        System.out.println("FIN INICIALIZACION");
    }
    
    @PostConstruct
    private void iniciarLlamadasWebService(){
        segundosEspera = Integer.parseInt(intervaloMuestra.getValor());
        tiempoEspera = segundosEspera * 1000;
        
        timer = new Timer();
        accionTimer = this.nuevaTareaWS(urlRaspberry.getValor());
        
        timer.scheduleAtFixedRate(accionTimer, 30000, tiempoEspera);
    }
            
    public boolean cambiarUrlRaspberry(String nuevaUrl){
        boolean correcto = true;
        
        if(!Utilidades.estaVacio(nuevaUrl)){
            if(urlRaspberry == null){
                urlRaspberry = new Variable();
                urlRaspberry.setNombre(Constantes.VARIABLE_URL_RASPBERRY);
            }
            
            urlRaspberry.setValor(nuevaUrl);
            Variable aux = variableControlador.merge(urlRaspberry);
            
            if(aux == null){
                correcto = false;
            }else{
                urlRaspberry = aux;
            }
            
        }else{
            correcto = false;
        }
        
        return correcto;
    }
    
    public boolean cambiarIntervaloMuestra(int nuevoIntervaloEnSegundos){
        boolean correcto = true;
        
        if(nuevoIntervaloEnSegundos >= Constantes.MIN_INTERVALO_MUESTRA){
            if(intervaloMuestra == null){
                intervaloMuestra = new Variable();
                intervaloMuestra.setNombre(Constantes.VARIABLE_INTERVALO_MUESTRA);
            } 
            
            intervaloMuestra.setValor(String.valueOf(nuevoIntervaloEnSegundos));
            Variable aux = variableControlador.merge(intervaloMuestra);
            
            if(aux == null){
                correcto = false;
            }else{
                intervaloMuestra = aux;
            }
            
            if(correcto){
                segundosEspera = nuevoIntervaloEnSegundos;
                tiempoEspera = segundosEspera * 1000;
                accionTimer.cancel();
                accionTimer = this.nuevaTareaWS(String.valueOf(tiempoEspera));
                timer.schedule(accionTimer, 5000, tiempoEspera);
            }
        }else{
            correcto = false;
        }
        
        return correcto;
    }
    
    public boolean cambiarNivelMaximoWS(Double nuevoNivelMaximo){
        boolean correcto = true;
        Variable maxNivelCO2 = variableControlador.findVariableByNombre(Constantes.VARIABLE_NIVEL_MAXIMO);
        
        if(nuevoNivelMaximo >= Constantes.MIN_NIVEL_MAXIMO){
            if(maxNivelCO2 == null){
                maxNivelCO2 = new Variable();
                maxNivelCO2.setNombre(Constantes.VARIABLE_INTERVALO_MUESTRA);
            } 
            
            maxNivelCO2.setValor(String.valueOf(nuevoNivelMaximo));
            Variable aux = variableControlador.merge(maxNivelCO2);
            
            if(aux == null){
                correcto = false;
            }
            
        }else{
            correcto = false;
        }
        
        return correcto;
    }

    public Variable getUrlRaspberry() {
        return urlRaspberry;
    }

    public Variable getIntervaloMuestra() {
        return intervaloMuestra;
    }
    
    private MuestraTask nuevaTareaWS(String urlRaspberry){
        if(urlRaspberry == null)
            urlRaspberry = "NULL";
        return new MuestraTask();
    }
    
    @PreDestroy
    private void alAcabar(){
        variableControlador = null;
        timer.cancel();
    }
}
